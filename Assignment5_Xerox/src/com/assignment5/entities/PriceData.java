/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

/**
 *
 * @author kesha
 */
public class PriceData {

    public PriceData() {
        this.totalSellingprice = 0;
        this.targetPrice = 0;
        this.quantity = 0;
    }

    public double getTotalSellingprice() {
        return totalSellingprice;
    }

    public void setTotalSellingprice(double totalSellingprice) {
        this.totalSellingprice = totalSellingprice;
    }

    public double getTargetPrice() {
        return targetPrice;
    }

    public void setTargetPrice(double targetPrice) {
        this.targetPrice = targetPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    private double totalSellingprice;
    private double targetPrice;
    private int quantity;
    
}
