/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

import java.util.ArrayList;

/**
 *
 * @author kasai
 */
public class Product {

    int productId;
    int minimumPrice;
    int maximumPrice;
    int targetPrice;
    ArrayList<Customer> customerList;
    int averageOrderWeightage; ////average of (order quantity * sales target price if sale price > target price)

    public int getAverageOrderWeightage() {
        return averageOrderWeightage;
    }

    public void setAverageOrderWeightage(int averageOrderWeightage) {
        this.averageOrderWeightage = averageOrderWeightage;
    }

    public ArrayList<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(ArrayList<Customer> customerList) {
        this.customerList = customerList;
    }
    public void addCustomer(Customer customer){
        this.customerList.add(customer);
    }
    public Product() {
        customerList = new ArrayList<Customer>();
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getMinimumPrice() {
        return minimumPrice;
    }

    public void setMinimumPrice(int minimumPrice) {
        this.minimumPrice = minimumPrice;
    }

    public int getMaximumPrice() {
        return maximumPrice;
    }

    public void setMaximumPrice(int maximumPrice) {
        this.maximumPrice = maximumPrice;
    }

    public int getTargetPrice() {
        return targetPrice;
    }

    public void setTargetPrice(int targetPrice) {
        this.targetPrice = targetPrice;
    }
}
