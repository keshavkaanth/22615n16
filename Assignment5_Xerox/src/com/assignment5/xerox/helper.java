/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.xerox;

import com.assignment5.entities.Customer;
import com.assignment5.entities.Item;
import com.assignment5.entities.Order;
import com.assignment5.entities.PriceData;
import com.assignment5.entities.Product;
import static com.assignment5.xerox.GateWay.DESC;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author kesha
 */
public class helper {

    public static Product GetMatchedProductFromProductCatalogCSV(int productId) {
        Map<Integer, Product> productMap = DataStore.getInstance().getProduct();
        return productMap.get(productId);
    }

    public static void AddCustomerDetailToProduct(Product finalProduct, int customerId, int productId, int salesPrice, int quantity, int orderId, int salesId) {
        Customer customer = new Customer();
        customer.setCustomerId(customerId);
        AddOrderDetailsToCustomer(customer, productId, salesPrice, quantity, orderId, salesId, customerId);
        finalProduct.getCustomerList().add(customer);
    }

    public static void AddOrderDetailsToCustomer(Customer customer, int productId, int salesPrice, int quantity, int orderId, int salesId, int customerId) {
        Item item = new Item(productId, salesPrice, quantity);
        Order order = new Order(orderId, salesId, customerId, item);
        customer.getOrdersList().add(order);
    }

    public static boolean checkForCustomerId(ArrayList<Customer> customerList, int customerId) {
        for (Customer customer : customerList) {
            if (customer.getCustomerId() == customerId) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkForOrderId(ArrayList<Order> ordersList, int orderId) {
        for (Order order : ordersList) {
            if (order.getOrderId() == orderId) {
                return true;
            }
        }
        return false;
    }

    public static Product getProductFromProductId(ArrayList<Product> productList, int productId) {
        for (Product product : productList) {
            if (product.getProductId() == productId) {
                return product;
            }
        }
        return null;
    }

    public static Customer getCustomerFromCustomerId(ArrayList<Customer> customerList, int customerId) {
        for (Customer customer : customerList) {
            if (customer.getCustomerId() == customerId) {
                return customer;
            }
        }
        return null;
    }

    public static Map<Integer, Integer> sortByComparator(Map<Integer, Integer> unsortMap, final boolean order) {

        List<Map.Entry<Integer, Integer>> list = new LinkedList<Map.Entry<Integer, Integer>>(unsortMap.entrySet());
        // Sorting the list based on values
        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {
            public int compare(Map.Entry<Integer, Integer> o1,
                    Map.Entry<Integer, Integer> o2) {
                if (order) {
                    return o1.getValue().compareTo(o2.getValue());
                } else {
                    return o2.getValue().compareTo(o1.getValue());

                }
            }
        });

        // Maintaining insertion order with the help of LinkedList
        Map<Integer, Integer> sortedMap = new LinkedHashMap<Integer, Integer>();
        for (Map.Entry<Integer, Integer> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }

    public static HashMap<Integer, PriceData> sortSummaryDataByComparator(HashMap<Integer, PriceData> unsortMap, final boolean order, final boolean isModifiedTable) {

        List<Map.Entry<Integer, PriceData>> list = new LinkedList<Map.Entry<Integer, PriceData>>(unsortMap.entrySet());

        // Sorting the list based on values
        Collections.sort(list, new Comparator<Map.Entry<Integer, PriceData>>() {
            public int compare(Map.Entry<Integer, PriceData> o1,
                    Map.Entry<Integer, PriceData> o2) {
                if (isModifiedTable) {
                    double averageSalePriceo1 = o1.getValue().getTotalSellingprice() / o1.getValue().getQuantity();
                    double modifiedTargetPriceo1 = getModifiedTargetPrice(o1.getValue().getTargetPrice(), averageSalePriceo1);
                    double differenceo1 = (modifiedTargetPriceo1 - averageSalePriceo1);
                    double averageSalePriceo2 = o2.getValue().getTotalSellingprice() / o2.getValue().getQuantity();
                    double modifiedTargetPriceo2 = getModifiedTargetPrice(o2.getValue().getTargetPrice(), averageSalePriceo2);
                    double differenceo2 = (modifiedTargetPriceo2 - averageSalePriceo2);
                    if (order) {
                        return Double.compare(differenceo1, differenceo2);
                    } else {
                        return Double.compare(differenceo2, differenceo1);
                    }
                } else {
                    double averageSalePriceo1 = o1.getValue().getTotalSellingprice() / o1.getValue().getQuantity();
                    double targetPriceo1 = o1.getValue().getTargetPrice();
                    double differenceo1 = (targetPriceo1 - averageSalePriceo1);
                    double averageSalePriceo2 = o2.getValue().getTotalSellingprice() / o2.getValue().getQuantity();
                    double targetPriceo2 = o2.getValue().getTargetPrice();
                    double differenceo2 = (targetPriceo2 - averageSalePriceo2);
                    if (order) {
                        return Double.compare(differenceo1, differenceo2);
                    } else {
                        return Double.compare(differenceo2, differenceo1);

                    }
                }

            }
        });

        // Maintaining insertion order with the help of LinkedList
        HashMap<Integer, PriceData> sortedMap = new LinkedHashMap<Integer, PriceData>();
        for (Map.Entry<Integer, PriceData> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }

    public static int calculateProductTotalQuantity(Product product) {
        int totalQuantity = 0;
        for (int i = 0; i < product.getCustomerList().size(); i++) {
            Customer customer = product.getCustomerList().get(i);
            for (int j = 0; j < customer.getOrdersList().size(); j++) {
                Order order = customer.getOrdersList().get(j);
                if (order.getItem().getSalesPrice() > product.getTargetPrice()) {
                    totalQuantity = totalQuantity + order.getItem().getQuantity();
                }
            }
        }
        return totalQuantity;
    }

    public static void displayAsTable(HashMap<Integer, PriceData> Productprofits, boolean isModifiedTable) {
        HashMap<Integer, PriceData> SortedProductprofits = sortSummaryDataByComparator(Productprofits, DESC, isModifiedTable);
        String leftAlignFormat = "";
        System.out.format("+-----------------+------+%n");
        if (isModifiedTable) {
            leftAlignFormat = "| %-15s | %-18f | %-15f | %-15f  | %-15f |%n";
            System.out.format("| Product ID     | Average Sale Price   | Modified Target Price  |  Difference  | Error Percent |%n");
        } else {
            leftAlignFormat = "| %-15s | %-18f | %-15f | %-15f | %-15f |%n";
            System.out.format("| Product ID     | Average Sale Price   | Target Price  |  Difference  | Error Percent  |%n");
        }
        System.out.format("+-----------------+------+%n");
        for (Map.Entry<Integer, PriceData> entry : SortedProductprofits.entrySet()) {
            double averageSalePrice = entry.getValue().getTotalSellingprice() / entry.getValue().getQuantity();
            int productId = entry.getKey();
            double targetPrice = entry.getValue().getTargetPrice();
            if (isModifiedTable) {
                double modifiedTargetPrice = getModifiedTargetPrice(targetPrice, averageSalePrice);
                double difference = (modifiedTargetPrice - averageSalePrice);
                double errorPercent = (difference / averageSalePrice) * 100;
                System.out.format(leftAlignFormat, productId, averageSalePrice, modifiedTargetPrice, difference, errorPercent);
            } else {
                double difference = (targetPrice - averageSalePrice);
                double errorPercent = (difference / averageSalePrice) * 100;
                System.out.format(leftAlignFormat, productId, averageSalePrice, targetPrice, difference, errorPercent);
            }
        }
        System.out.format("+-----------------+------+%n");
    }

    public static boolean checkForProductId(ArrayList<Product> productArrayList, int productId) {
        for (Product product : productArrayList) {
            if (product.getProductId() == productId) {
                return true;
            }
        }
        return false;
    }

    public static double getModifiedTargetPrice(double targetPrice, double averageSalePrice) {
        double modifiedTargetPrice = targetPrice;
        if (targetPrice < (averageSalePrice - (0.05 * averageSalePrice))) {
            modifiedTargetPrice = averageSalePrice - (0.05 * averageSalePrice);
        } else if (targetPrice > (averageSalePrice + (0.05 * averageSalePrice))) {
            modifiedTargetPrice = averageSalePrice + (0.05 * averageSalePrice);
        }
        return modifiedTargetPrice;
    }

}
