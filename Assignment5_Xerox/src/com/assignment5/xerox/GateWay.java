/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.xerox;

import com.assignment5.entities.Customer;
import com.assignment5.entities.Item;
import com.assignment5.entities.Order;
import com.assignment5.entities.PriceData;
import com.assignment5.entities.Product;
import com.assignment5.entities.SalesPerson;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 *
 * @author kasai
 */
public class GateWay {

    public static boolean ASC = true;
    public static boolean DESC = false;

    public static void main(String args[]) throws IOException {

        DataGenerator generator = DataGenerator.getInstance();

        //Below is the sample for how you can use reader. you might wanna 
        //delete it once you understood.
        // System.out.println("_____________________________________________________________");
        DataReader productReader = new DataReader(generator.getProductCataloguePath());
        String[] prodRow;
        productReader.getFileHeader();
        while ((prodRow = productReader.getNextRow()) != null) {
            //printRow(prodRow);
            generateProduct(prodRow);
        }
        DataReader orderReader = new DataReader(generator.getOrderFilePath());
        String[] orderRow;
        orderReader.getFileHeader();
        while ((orderRow = orderReader.getNextRow()) != null) {
            //printRow(orderRow);
            generateOrder(orderRow);
        }
        System.out.println("");
        System.out.println("Question 1- Top 3 best negotiated products");
        top3NegotiatedProducts();
        System.out.println("");
        System.out.println("Question 2 - Top 3 best customers");
        top3bestCustomers();
        System.out.println("");
        System.out.println("Question 3 - Top 3 best sales persons");
        topThreeBestSalesPeople();
        System.out.println("Question 5- Summary of Original Table sorted by difference in DESC");
        priceanalysis(false);
        System.out.println("Question 5- Summary of Modififed Table sorted by difference in DESC");
        priceanalysis(true);

    }

    public static void printRow(String[] row) {
        for (String row1 : row) {
            System.out.print(row1 + ", ");
        }
        System.out.println("");
    }

    private static void generateProduct(String[] prodRow) {
        Product product = new Product();
        product.setProductId(Integer.parseInt(prodRow[0]));
        product.setMinimumPrice(Integer.parseInt(prodRow[1]));
        product.setMaximumPrice(Integer.parseInt(prodRow[2]));
        product.setTargetPrice(Integer.parseInt(prodRow[3]));
        DataStore.getInstance().getProduct().put(Integer.parseInt(prodRow[0]), product);
    }

    private static void generateOrder(String[] orderRow) {
        ///// structure created after reading the sales data CSV
        int salesId = Integer.parseInt(orderRow[4]);
        Map<Integer, SalesPerson> salesPersonMap = DataStore.getInstance().getSalesperson();
        int orderId = Integer.parseInt(orderRow[0]);
        int productId = Integer.parseInt(orderRow[2]);
        int customerId = Integer.parseInt(orderRow[5]);
        int salesPrice = Integer.parseInt(orderRow[6]);
        int quantity = Integer.parseInt(orderRow[3]);
        if (salesPersonMap.containsKey(salesId)) {
            SalesPerson salesPerson = salesPersonMap.get(salesId);
            if (helper.checkForProductId(salesPerson.getProductList(), productId)) {
                Product productObj = helper.getProductFromProductId(salesPerson.getProductList(), productId);
                if (helper.checkForCustomerId(productObj.getCustomerList(), customerId)) {
                    Customer matchedCustomer = helper.getCustomerFromCustomerId(productObj.getCustomerList(), customerId);
                    if (!helper.checkForOrderId(matchedCustomer.getOrdersList(), orderId)) {
                        helper.AddOrderDetailsToCustomer(matchedCustomer, productId, salesPrice, quantity, orderId, salesId, customerId);
                    }
                } else {
                    helper.AddCustomerDetailToProduct(productObj, customerId, productId, salesPrice, quantity, orderId, salesId);
                }
            } else {
                Product matchedProduct = helper.GetMatchedProductFromProductCatalogCSV(productId);
                Product finalProduct = new Product();
                finalProduct.setProductId(matchedProduct.getProductId());
                finalProduct.setMaximumPrice(matchedProduct.getMaximumPrice());
                finalProduct.setMinimumPrice(matchedProduct.getMinimumPrice());
                finalProduct.setTargetPrice(matchedProduct.getTargetPrice());

                helper.AddCustomerDetailToProduct(finalProduct, customerId, productId, salesPrice, quantity, orderId, salesId);
                salesPerson.getProductList().add(finalProduct);
            }
        } else {
            SalesPerson salesPerson = new SalesPerson();
            salesPerson.setSalesId(salesId);
            Product matchedProduct = helper.GetMatchedProductFromProductCatalogCSV(productId);
            Product finalProduct = new Product();
            finalProduct.setProductId(matchedProduct.getProductId());
            finalProduct.setMaximumPrice(matchedProduct.getMaximumPrice());
            finalProduct.setMinimumPrice(matchedProduct.getMinimumPrice());
            finalProduct.setTargetPrice(matchedProduct.getTargetPrice());
            helper.AddCustomerDetailToProduct(finalProduct, customerId, productId, salesPrice, quantity, orderId, salesId);
            salesPerson.getProductList().add(finalProduct);
            salesPersonMap.put(salesId, salesPerson);
        }
        ////to do serialize the salespersonmap as json
    }

    static void top3bestCustomers() {
        Map<Integer, SalesPerson> sales = DataStore.getInstance().getSalesperson();
        HashMap<Integer, Integer> customerprofits = new HashMap<Integer, Integer>();
        for (Map.Entry<Integer, SalesPerson> sale : sales.entrySet()) {

            for (Product product : sale.getValue().getProductList()) {
                int tp = product.getTargetPrice();
                for (Customer customer : product.getCustomerList()) {
                    int diff = 0;
                    for (Order order : customer.getOrdersList()) {
                        int sp = order.getItem().getSalesPrice();
                        int quantity = order.getItem().getQuantity();
                        diff = diff + ((Math.abs(sp - tp)) * quantity);
                    }
                    if (customerprofits.containsKey(customer.getCustomerId())) {

                        customerprofits.put(customer.getCustomerId(), customerprofits.get(customer.getCustomerId()) + diff);
                    } else {
                        customerprofits.put(customer.getCustomerId(), diff);
                    }
                }

            }

        }
        Map<Integer, Integer> sortedprofits = helper.sortByComparator(customerprofits, ASC);
        System.out.println("Three best customers:");
        int count = 0;
        for (Map.Entry<Integer, Integer> entry : sortedprofits.entrySet()) {
            if (count >= 3) {
                break;
            }
            System.out.println("Cutomer ID:" + entry.getKey() + "   Total difference: " + entry.getValue());
            count++;
        }
    }

    public static void priceanalysis(boolean isModifedTable) {
        Map<Integer, SalesPerson> sales = DataStore.getInstance().getSalesperson();
        HashMap<Integer, PriceData> Productprofits = new HashMap<Integer, PriceData>();
        for (Map.Entry<Integer, SalesPerson> sale : sales.entrySet()) {

            for (Product product : sale.getValue().getProductList()) {
                PriceData priceAnalysis = new PriceData();
                priceAnalysis.setTargetPrice(product.getTargetPrice());
                for (Customer customer : product.getCustomerList()) {

                    for (Order order : customer.getOrdersList()) {
                        priceAnalysis.setTotalSellingprice((priceAnalysis.getTotalSellingprice()) + (order.getItem().getSalesPrice() * order.getItem().getQuantity()));
                        priceAnalysis.setQuantity(priceAnalysis.getQuantity() + order.getItem().getQuantity());

                    }
                }
                if (Productprofits.containsKey(product.getProductId())) {
                    PriceData priceAnalysisExisting = Productprofits.get(product.getProductId());
                    priceAnalysis.setQuantity(priceAnalysisExisting.getQuantity() + priceAnalysis.getQuantity());
                    priceAnalysis.setTotalSellingprice(priceAnalysisExisting.getTotalSellingprice() + priceAnalysis.getTotalSellingprice());
                    Productprofits.put(product.getProductId(), priceAnalysis);

                } else {
                    Productprofits.put(product.getProductId(), priceAnalysis);
                }

            }
        }
        helper.displayAsTable(Productprofits, isModifedTable);
        if (isModifedTable) {
            System.out.println("Section 1 Products - Average Sales price lower than modified target price sorted by difference in DESC");
        } else {
            System.out.println("Section 1 Products - Average Sales price lower than target price sorted by difference in DESC");
        }
        HashMap<Integer, PriceData> section1Products = new HashMap<Integer, PriceData>();
        for (Entry<Integer, PriceData> entry : Productprofits.entrySet()) {
            Integer key = entry.getKey();
            PriceData value = entry.getValue();
            double averageSalePrice = value.getTotalSellingprice() / value.getQuantity();
            double targetPrice = value.getTargetPrice();
            if (isModifedTable) {
                if (averageSalePrice < helper.getModifiedTargetPrice(targetPrice, averageSalePrice)) {
                    section1Products.put(key, value);
                }
            } else {
                if (averageSalePrice < targetPrice) {
                    section1Products.put(key, value);
                }
            }

        }

        helper.displayAsTable(section1Products, isModifedTable);
        if (isModifedTable) {
            System.out.println(
                    "Section 2 Products - Average Sales price higher than modified target price sorted by difference in DESC");
        } else {
            System.out.println(
                    "Section 2 Products - Average Sales price higher than target price sorted by difference in DESC");
        }
        HashMap<Integer, PriceData> section2Products = new HashMap<Integer, PriceData>();
        for (Entry<Integer, PriceData> entry
                : Productprofits.entrySet()) {
            Integer key = entry.getKey();
            PriceData value = entry.getValue();
            double averageSalePrice = value.getTotalSellingprice() / value.getQuantity();
            double targetPrice = value.getTargetPrice();
            if (isModifedTable) {
                if (averageSalePrice > helper.getModifiedTargetPrice(targetPrice, averageSalePrice)) {
                    section2Products.put(key, value);
                }
            } else {
                if (averageSalePrice > targetPrice) {
                    section2Products.put(key, value);
                }
            }

        }

        helper.displayAsTable(section2Products, isModifedTable);

    }

    private static void top3NegotiatedProducts() {
        Map<Integer, SalesPerson> salesData = DataStore.getInstance().getSalesperson();
        HashMap<Integer, Integer> productIdWeightageMap = new HashMap<Integer, Integer>();
        for (Map.Entry<Integer, SalesPerson> entry : salesData.entrySet()) {
            Integer salesId = entry.getKey();
            SalesPerson salesPerson = entry.getValue();
            ArrayList<Product> productList = salesPerson.getProductList();
            for (int i = 0; i < productList.size(); i++) {
                int productQuantity = 0;
                Product product = productList.get(i);
                productQuantity = helper.calculateProductTotalQuantity(product);
                product.setAverageOrderWeightage(productQuantity);
                if (productIdWeightageMap.containsKey(product.getProductId())) {
                    int weightageStored = productIdWeightageMap.get(product.getProductId());
                    weightageStored = weightageStored + productQuantity;
                    productIdWeightageMap.put(product.getProductId(), weightageStored);
                } else {
                    productIdWeightageMap.put(product.getProductId(), productQuantity);
                }
            }
        }
        int count = 0;
        int iterationCount = 0;
        Map<Integer, Integer> productIdMapSorted = helper.sortByComparator(productIdWeightageMap, DESC);
        ArrayList<Integer> quantityArrayList = new ArrayList<Integer>(productIdMapSorted.values());
        for (Map.Entry<Integer, Integer> entry : productIdMapSorted.entrySet()) {
            if (count < 3) {
                Integer key = entry.getKey();
                Integer value = entry.getValue();
                int currentValue = quantityArrayList.get(iterationCount);
                int nextValue = quantityArrayList.get(iterationCount + 1);
                if (nextValue != currentValue) {
                    count++;
                }
                System.out.println("Product Id " + key + " Total Sales Quantity " + value);
                iterationCount++;
            }
        };
    }

    public static void topThreeBestSalesPeople() {

        Map<Integer, SalesPerson> sales = DataStore.getInstance().getSalesperson();
        HashMap<Integer, Integer> salespersonprofits = new HashMap<Integer, Integer>();
        for (Map.Entry<Integer, SalesPerson> sale : sales.entrySet()) {
            int profit = 0;
            for (Product product : sale.getValue().getProductList()) {
                int tp = product.getTargetPrice();
                for (Customer customer : product.getCustomerList()) {
                    for (Order order : customer.getOrdersList()) {
                        int sp = order.getItem().getSalesPrice();
                        int quantity = order.getItem().getQuantity();
                      //  profit = profit + ((sp - tp) * quantity);
                          profit = profit + (Math.abs(sp - tp) * quantity);  //considering the absolute value logic
                    }
                }
            }
            if (salespersonprofits.containsKey(sale.getValue().getSalesId())) {

                salespersonprofits.put(sale.getValue().getSalesId(), salespersonprofits.get(sale.getValue().getSalesId()) + profit);
            } else {
                salespersonprofits.put(sale.getValue().getSalesId(), profit);
            }
        }
        Map<Integer, Integer> sortedprofits = helper.sortByComparator(salespersonprofits, DESC);
        System.out.println("Top 3 Best Sales Person:");
        int count = 0;
        for (Map.Entry<Integer, Integer> entry : sortedprofits.entrySet()) {
            if (count >= 3) {
                break;
            }
            System.out.println("Sales Person ID:" + entry.getKey() + " Total Profit: " + entry.getValue());
            count++;
        }

        System.out.println("");
        System.out.println("Question 4 - Our total profit for the year :");

        int sum = 0;
        for (Map.Entry<Integer, Integer> entry : sortedprofits.entrySet()) {
            sum += entry.getValue();
            //  System.out.println("Sales Person ID:" + entry.getKey() + " Total Profit for this sales person: " + entry.getValue());

        }
        System.out.println("Total Profit\t" + sum);
    }

}
