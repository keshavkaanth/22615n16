/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab7.analytics;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import static lab7.analytics.AnalysisHelper.sortByValue;
import lab7.entities.Comment;
import lab7.entities.Post;
import lab7.entities.User;

/**
 *
 * @author harshalneelkamal
 */
public class AnalysisHelper {

    // find user with Most Likes
    // TODO
    public static boolean ASC = true;
    public static boolean DESC = false;

    // find 5 comments which have the most likes
    // TODO
    public void getTopFiveLikedComments() {
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        List<Comment> commentList = new ArrayList<Comment>(comments.values());
        Comparator<Comment> test = new Comparator<Comment>() {
            @Override
            public int compare(Comment c1, Comment c2) {
                return c2.getLikes() - c1.getLikes();
            }
        };
        Collections.sort(commentList, test);
        System.out.println("\n5 comments which have the most likes");
        for (int i = 0; i < commentList.size() && i < 5; i++) {
            System.out.println("\t" + commentList.get(i));
        }
    }

// Question 1 --> Find Average number of likes per comment.
    public void averageLikesPerComments() {
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        List<Comment> commentList = new ArrayList<Comment>(comments.values());
        int total = 0;
        int sum = 0;
        float average = 0;
        total = commentList.size();
        for (Comment comment : commentList) {
            sum += comment.getLikes();
        }
        average = sum / total;
        System.out.println("\nQuestion No 1:" + "\n" + "\tAverage number of likes:" + average + "\n" + "\tTotal number of likes: " + sum + "\n\tTotal number of comments: " + total);
    }
// Question 3 --> Find the post with most comments.

    public void getPostWithMostComments() {
        Map<Integer, Post> Posts = DataStore.getInstance().getPosts();
        int max = 0;
        Map<Integer, Integer> Max = new HashMap<Integer, Integer>();

        for (Integer key : Posts.keySet()) {
            Post post = Posts.get(key);
            if (post.getComments().size() >= max) {
                max = post.getComments().size();
                Max.put(max, post.getPostId());
            }
        }
        System.out.println("\nQuestion 3:\n ");
        for (Entry<Integer, Post> entry : Posts.entrySet()) {
            if (max == entry.getValue().getComments().size()) {
                System.out.println("    Post with most Comments Post ID: " + entry.getValue().getPostId() + "  Comments: " + max);
            }
        }
    }
//Question 4 --> Top 5 inactive users based on total posts number.

    public void getInactiveUsersBasedonTotalPosts() {
        Map<Integer, Post> Posts = DataStore.getInstance().getPosts();
        HashMap<Integer, Integer> UserIdPosts = new HashMap<Integer, Integer>();
        for (Integer key : Posts.keySet()) {
            Post post = Posts.get(key);

            if (UserIdPosts.containsKey(post.getUserId())) {
                UserIdPosts.put(post.getUserId(), UserIdPosts.get(post.getUserId()) + 1);
            } else {
                UserIdPosts.put(post.getUserId(), 1);
            }
        }

        Map<Integer, Integer> UserIdPostsSorted = sortByValue(UserIdPosts);
        System.out.println("\nQuestion 4: \n    Most inactive users based on no of posts:");
        int i = 0;
        int previousPostCount = -1;
        for (Map.Entry<Integer, Integer> entry : UserIdPostsSorted.entrySet()) {
            if (previousPostCount != entry.getValue()) {
                i++;
            }
            previousPostCount = entry.getValue();
            System.out.println("     User  " + entry.getKey() + " has total " + entry.getValue() + " no of Posts.");
            if (i == 6) {
                break;
            }

        }

    }

    public static HashMap<Integer, Integer> sortByValue(HashMap<Integer, Integer> hm) {
        // Create a list from elements of HashMap 
        List<Map.Entry<Integer, Integer>> list
                = new LinkedList<Map.Entry<Integer, Integer>>(hm.entrySet());

        // Sort the list 
        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {
            public int compare(Map.Entry<Integer, Integer> o1,
                    Map.Entry<Integer, Integer> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        // put data from sorted list to hashmap  
        HashMap<Integer, Integer> temp = new LinkedHashMap<Integer, Integer>();
        for (Map.Entry<Integer, Integer> aa : list) {
            temp.put(aa.getKey(), aa.getValue());
        }
        return temp;
    }

//Question 6 --> Top 5 inactive users overall (sum of comments, posts and likes)
    public void getOverallInactiveUsers() {
        Map<Integer, Post> Posts = DataStore.getInstance().getPosts();
        Map<Integer, Comment> Comments = DataStore.getInstance().getComments();
        List<Comment> commentList1 = new ArrayList<Comment>(Comments.values());

        List<Post> postList = new ArrayList<Post>(Posts.values());
        HashMap<Integer, Integer> UserIdPostsComments = new HashMap<Integer, Integer>();

        for (Post post : postList) {
            if (UserIdPostsComments.containsKey(post.getUserId())) {
                UserIdPostsComments.put(post.getUserId(), UserIdPostsComments.get(post.getUserId()) + 1);
            } else {
                UserIdPostsComments.put(post.getUserId(), 1);
            }

        }

        for (Comment comment : commentList1) {
            if (UserIdPostsComments.containsKey(comment.getUserId())) {
                UserIdPostsComments.put(comment.getUserId(), UserIdPostsComments.get(comment.getUserId()) + 1 + comment.getLikes());
            } else {
                UserIdPostsComments.put(comment.getUserId(), 1);
            }

        }
        Map<Integer, Integer> UserIdPostsSorted = sortByValue(UserIdPostsComments);
        System.out.println("\nQuestion 6: \n    Top 5 inactive users:");
        int i = 0;

        for (Map.Entry<Integer, Integer> entry : UserIdPostsSorted.entrySet()) {
            if (i < 5) {
                System.out.println("     UserId = " + entry.getKey() + ", No of Posts, Comments and Likes = " + entry.getValue());
                i++;

            }

        }
        System.out.println("\nQuestion 7: \n    Top 5 proactive users:");
        int count = 0;
        for (Map.Entry<Integer, Integer> entry : UserIdPostsSorted.entrySet()) {

            int j = UserIdPostsSorted.size() - 5;

            if (count >= j) {
                System.out.println("     UserId = " + entry.getKey() + ", Total no of Posts, Comments and Likes = " + entry.getValue());
                if (count == UserIdPostsSorted.size()) {
                    break;
                };
            }
            count++;

        }

    }

    // find the post with most liked comments
    public void getPostWithMostLikedComments() {
        System.out.println("Question 2:\nPosts with most liked comments:");
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        List<Comment> commentList = new ArrayList<Comment>(comments.values());
        Collections.sort(commentList, new Comparator<Comment>() {
            @Override
            public int compare(Comment c1, Comment c2) {
                return c2.getLikes() - c1.getLikes();
            }
        });
        int commentSize = commentList.size();
        for (int i = 0; i < commentSize; i++) {
            System.out.println("\t" + commentList.get(i));
            if ((i + 1) < commentSize && commentList.get(i + 1).getLikes() < commentList.get(i).getLikes()) {
                break;
            }
        }
        System.out.println("");
    }

// top 5 inactive users based on the total comments they created
    public void getTop5InactiveUsersBasedOnComments() {
        Map<Integer, User> usersMap = DataStore.getInstance().getUsers();
        Map<Integer, User> inactiveUsersMap = sortByComparator(usersMap, ASC);
        System.out.println("Question 5:");
        int count = 0;
        int previousPostCount = -1;
        for (Map.Entry<Integer, User> entry : inactiveUsersMap.entrySet()) {
            if (previousPostCount != entry.getValue().getComments().size()) {
                count++;
            }
            previousPostCount = entry.getValue().getComments().size();
            System.out.println("\t" + entry.getValue());
            if (count == 5) {
                break;
            }
        }

    }

    private static Map<Integer, User> sortByComparator(Map<Integer, User> unsortMap, final boolean order) {

        List<Map.Entry<Integer, User>> list = new LinkedList<Map.Entry<Integer, User>>(unsortMap.entrySet());

        // Sorting the list based on values
        Collections.sort(list, new Comparator<Map.Entry<Integer, User>>() {
            public int compare(Map.Entry<Integer, User> o1,
                    Map.Entry<Integer, User> o2) {
                if (order) {
                    return o1.getValue().getComments().size() - o2.getValue().getComments().size();
                } else {
                    return o2.getValue().getComments().size() - o1.getValue().getComments().size();

                }
            }
        });

        // Maintaining insertion order with the help of LinkedList
        Map<Integer, User> sortedMap = new LinkedHashMap<Integer, User>();
        for (Map.Entry<Integer, User> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }

}
