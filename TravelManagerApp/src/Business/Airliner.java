/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author kesha
 */
public class Airliner {

    private String name;

    private int noOfFlightsInFleet;
    private AirplaneFleet airplaneFleet;
    private int totalNumberOfFlightPerDay;
    private int turnover;

    public int getTurnover() {
        return turnover;
    }

    public void setTurnover(int turnover) {
        this.turnover = turnover;
    }

    public int getTotalNumberOfFlightPerDay() {
        return totalNumberOfFlightPerDay;
    }

    public void setTotalNumberOfFlightPerDay(int totalNumberOfFlightPerDay) {
        this.totalNumberOfFlightPerDay = totalNumberOfFlightPerDay;
    }

    public int getNoOfFlightsInFleet() {
        return noOfFlightsInFleet;
    }

    public void setNoOfFlightsInFleet(int noOfFlightsInFleet) {
        this.noOfFlightsInFleet = noOfFlightsInFleet;
    }

    public AirplaneFleet getAirplaneFleet() {
        return airplaneFleet;
    }

    public void setAirplaneFleet(AirplaneFleet airplaneFleet) {
        this.airplaneFleet = airplaneFleet;
    }

    public Airliner() {
        airplaneFleet = new AirplaneFleet();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
