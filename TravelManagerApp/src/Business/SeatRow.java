/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author kesha
 */
public class SeatRow {

    public Seat getLeftWindow() {
        return leftWindow;
    }

    public void setLeftWindow(Seat leftWindow) {
        this.leftWindow = leftWindow;
    }

    public Seat getRightWindow() {
        return rightWindow;
    }

    public void setRightWindow(Seat rightWindow) {
        this.rightWindow = rightWindow;
    }

    public Seat getLeftAisle() {
        return leftAisle;
    }

    public void setLeftAisle(Seat leftAisle) {
        this.leftAisle = leftAisle;
    }

    public Seat getRightAisle() {
        return rightAisle;
    }

    public void setRightAisle(Seat rightAisle) {
        this.rightAisle = rightAisle;
    }

    public Seat getLeftMiddle() {
        return leftMiddle;
    }

    public void setLeftMiddle(Seat leftMiddle) {
        this.leftMiddle = leftMiddle;
    }

    public Seat getRightMiddle() {
        return rightMiddle;
    }

    public void setRightMiddle(Seat rightMiddle) {
        this.rightMiddle = rightMiddle;
    }
    private Seat leftWindow;
    private Seat rightWindow;
    private Seat leftAisle;
    private Seat rightAisle;
    private Seat leftMiddle;
    private Seat rightMiddle;

    public SeatRow(int i) {
        this.leftAisle = new Seat(i, "LA");
        this.rightAisle = new Seat(i, "RA");
        this.leftWindow = new Seat(i, "LW");
        this.rightWindow = new Seat(i, "RW");
        this.leftMiddle = new Seat(i, "LM");
        this.rightMiddle = new Seat(i, "RM");
    }

}
