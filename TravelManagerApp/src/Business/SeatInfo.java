/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author kshar
 */
public class SeatInfo {

   ArrayList<SeatRow> RowList;

    public ArrayList<SeatRow> getRowList() {
        return RowList;
    }

    public void setRowList(ArrayList<SeatRow> RowList) {
        this.RowList = RowList;
    }

   

    public SeatInfo() {
        RowList=new ArrayList<>();
        for(int i=0;i<25;i++)
        {
            SeatRow row=new SeatRow(i+1);
            RowList.add(row);
        }
    }

}
