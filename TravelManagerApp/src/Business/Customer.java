/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author kesha
 */
public class Customer {

    private String name;
    private String country;
    private String age;
    private FlightTrip flightTrip;
    private String assignedSeat;

    public FlightTrip getFlightTrip() {
        return flightTrip;
    }

    public void setFlightTrip(FlightTrip flightTrip) {
        this.flightTrip = flightTrip;
    }

    public String getAssignedSeat() {
        return assignedSeat;
    }

    public void setAssignedSeat(String assignedSeat) {
        this.assignedSeat = assignedSeat;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

}
