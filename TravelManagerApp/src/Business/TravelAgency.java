/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kesha
 */
public class TravelAgency {

    private ArrayList<Customer> customerList;
    private ArrayList<Airliner> airlinerList;
    // Master schedule object is replaced by flight schedule object

    public ArrayList<Airliner> getAirlinerList() {
        return airlinerList;
    }

    public void setAirlinerList(ArrayList<Airliner> AirlinerList) {
        this.airlinerList = AirlinerList;
    }

    public TravelAgency() {
        customerList = new ArrayList<Customer>();
        airlinerList = new ArrayList<Airliner>();
    }

    public ArrayList<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(ArrayList<Customer> supplierList) {
        this.customerList = supplierList;
    }

}
