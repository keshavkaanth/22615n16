/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author kshar
 */
public class AirplaneFleet {

    private ArrayList<Flight> fleetOfFlights;
    private String noOfFlightsInAirliner;
    private String lastUpdatedFleetTime;

    public String getLastUpdatedFleetTime() {
        return lastUpdatedFleetTime;
    }

    public void setLastUpdatedFleetTime(String lastUpdatedFleetTime) {
        this.lastUpdatedFleetTime = lastUpdatedFleetTime;
    }
    public String getNoOfFlightsInAirliner() {
        return noOfFlightsInAirliner;
    }

    public void setNoOfFlightsInAirliner(String noOfFlightsInAirliner) {
        this.noOfFlightsInAirliner = noOfFlightsInAirliner;
    }

    public ArrayList<Flight> getFleetOfFlights() {
        return fleetOfFlights;
    }

    public void setFleetOfFlights(ArrayList<Flight> fleetOfFlights) {
        this.fleetOfFlights = fleetOfFlights;
    }

    public AirplaneFleet() {
        fleetOfFlights = new ArrayList<Flight>();
    }
    public void addFlight(Flight flight){
        
        this.fleetOfFlights.add(flight);
        
    }

}
