/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author kesha
 */
public class Seat {

    public boolean isIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
    private boolean isAvailable;
    private String number;
    private String bookedBY;

    public String getBookedBY() {
        return bookedBY;
    }

    public void setBookedBY(String bookedBY) {
        this.bookedBY = bookedBY;
    }
    public Seat(int i, String pos) {
        this.isAvailable=true;
        this.number= String.valueOf(i)+"-"+pos;
              
    }

}
