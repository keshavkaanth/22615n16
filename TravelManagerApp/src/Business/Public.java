/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Enumeration;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author kshar
 */
public class Public {

    public static String lastUpdatedTime = getDateAndTime();

    public static String getDateAndTime() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);
    }

    public static void inputVerifierRulesForNumber(JTextField... jTextFieldArray) {
        for (JTextField jTextField : jTextFieldArray) {
            jTextField.setInputVerifier(new InputVerifier() {
                @Override
                public boolean verify(JComponent jc) {
                    String text = ((JTextField) jc).getText();
                    boolean isNumeric = isNumeric(text);
                    if (isNumeric || text.equals("")) {
                        return true;
                    } else {
                        JOptionPane.showMessageDialog(jc, "Please enter integer for field", "Error Dialog",
                                JOptionPane.ERROR_MESSAGE);
                        return false;
                    }
                }
            });
        }
    }
    
    public static void ClearAllFields(JTextField... jTextFieldArray) {
        for(JTextField jTextField : jTextFieldArray){
            jTextField.setText("");
        }
    }

    public static boolean isNumeric(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static String getSelectedButtonText(ButtonGroup buttonGroup) {
        for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements();) {
            AbstractButton button = buttons.nextElement();

            if (button.isSelected()) {
                return button.getText();
            }
        }

        return null;
    }

}
