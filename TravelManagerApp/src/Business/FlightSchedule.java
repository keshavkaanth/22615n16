/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author kshar
 */
public class FlightSchedule {

    private ArrayList<FlightTrip> schedule;
    
    public ArrayList<FlightTrip> getSchedule() {
        return schedule;
    }

    public void setSchedule(ArrayList<FlightTrip> schedule) {
        this.schedule = schedule;
    }

    public FlightSchedule() {
        schedule = new ArrayList<FlightTrip>();
    }
    public void addTrip(FlightTrip trip){
        this.schedule.add(trip);
    }


   
}
