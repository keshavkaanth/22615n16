/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author kshar
 */
public class FlightTrip {

    private String airlinerName;
    private int serialNo;
    private String modelNo;
    private String tripId;

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }
    public String getModelNo() {
        return modelNo;
    }

    public void setModelNo(String modelNo) {
        this.modelNo = modelNo;
    }

    public int getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(int serialNo) {
        this.serialNo = serialNo;
    }

    public String getAirlinerName() {
        return airlinerName;
    }

    public void setAirlinerName(String airlinerName) {
        this.airlinerName = airlinerName;
    }
    private String from;

    private String to;
    private String departure;
    private String arrival;
    private int price;
    private SeatInfo seatInfo;
    private int seatsAvailable = 150;

    public int getSeatsAvailable() {
        return seatsAvailable;
    }

    public void setSeatsAvailable(int seatsAvailable) {
        this.seatsAvailable = seatsAvailable;
    }

    public FlightTrip(String from, String to, String departure, String arrival, int price) {
        this.from = from;
        this.to = to;
        this.departure = departure;
        this.arrival = arrival;
        this.seatInfo = new SeatInfo();
        this.price = price;
    }

    public void decrementSeat() {
        this.seatsAvailable++;
    }

    public FlightTrip() {
        this.seatInfo = new SeatInfo();

    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Date getDeparture() {
        Date date = null;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
            date = formatter.parse(departure);
            return date;
        } catch (ParseException ex) {
            JOptionPane.showMessageDialog(null, "Invalid Date format");
            Logger.getLogger(FlightTrip.class.getName()).log(Level.SEVERE, null, ex);
        }
        return date;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public Date getArrival() {
        Date date = null;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
            date = formatter.parse(arrival);
            return date;
        } catch (ParseException ex) {
            JOptionPane.showMessageDialog(null, "Invalid Date format");
            Logger.getLogger(FlightTrip.class.getName()).log(Level.SEVERE, null, ex);
        }
        return date;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public SeatInfo getSeatInfo() {
        return seatInfo;
    }

    public void setSeatInfo(SeatInfo seatInfo) {
        this.seatInfo = seatInfo;
    }

    @Override
    public String toString() {
        return getFrom();
    }
}

//////      Date dNow = new Date( );
//      SimpleDateFormat ft = 
//      new SimpleDateFormat ("hh:mm:ss a");
//
//      System.out.println("Current Date: " + ft.format(dNow));

